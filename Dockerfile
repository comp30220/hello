FROM openjdk:8-jre-alpine
ENV NAME=Docky
COPY target/hello-*.jar /hello.jar
CMD java -cp /hello.jar Hello $NAME
